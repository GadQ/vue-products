import Vue from 'vue';
import App from './App.vue';
import store from './store/store';
import * as Storage from './utilities/storage';

Vue.config.productionTip = false;

store.subscribe((mutation, state) => {
    const store = {
        products: state.products,
        categories: state.categories,
        productLastIndex: state.productLastIndex,
        activeTab: state.activeTab
    };
    Storage.saveStorage(store);
});

Vue.directive('click-outside', {
    bind: function (el, binding, vnode) {
        document.body.addEventListener('click', () => {
            vnode.context[binding.expression]();
        });
    }
});


new Vue({
    render: h => h(App),
    store,
    beforeCreate() {
        this.$store.commit('initialiseStore');
    }
}).$mount('#app');

