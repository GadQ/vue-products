const HEX_COLOR_FORMAT_REGEX = /^(?:#?)([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})$/i;

export function hexToRgb(hex) {
    let rgb = [0, 0, 0];
    const hexArr = HEX_COLOR_FORMAT_REGEX.exec(hex);

    if (hexArr) {
        rgb = hexArr.slice(1, 4).map(channel => parseInt(channel, 16));
    }

    return rgb;
}

export function isValidHex(hex) {
    return HEX_COLOR_FORMAT_REGEX.test(hex);
}

export function rgbToHex(rgb) {
    return `#${rgb.map(channel => channel.toString(16).padStart(2, '0')).join('')}`;
}

export function rgbToHsl(r, g, b) {
    r /= 255;
    g /= 255;
    b /= 255;

    const max = Math.max(r, g, b);
    const min = Math.min(r, g, b);

    let h;
    let s;
    let l = (max + min) / 2;

    if (max === min) {
        h = s = 0; // achromatic
    } else {
        const d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

        switch (max) {
            case r:
                h = (g - b) / d + (g < b ? 6 : 0);
                break;
            case g:
                h = (b - r) / d + 2;
                break;
            case b:
                h = (r - g) / d + 4;
                break;
        }

        h /= 6;
    }

    return [h * 360, s * 100, l * 100].map(Math.round);
}

export function hslToRgb(h, s, l) {
    let r;
    let g;
    let b;

    h /= 360;
    s /= 100;
    l /= 100;

    if (s === 0) {
        r = g = b = l; // achromatic
    } else {
        const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        const p = 2 * l - q;

        r = hueToRgb(p, q, h + 1 / 3);
        g = hueToRgb(p, q, h);
        b = hueToRgb(p, q, h - 1 / 3);
    }

    return [r, g, b].map(channel => Math.round(channel * 255));
}

function hueToRgb(p, q, t) {
    if (t < 0) t += 1;
    if (t > 1) t -= 1;
    if (t < 1 / 6) return p + (q - p) * 6 * t;
    if (t < 1 / 2) return q;
    if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
    return p;
}

