export const STORAGE_KEY = 'vue-products';

export function saveStorage(state) {
    localStorage.setItem(STORAGE_KEY, JSON.stringify(state));
}

export function loadStorage() {
    const state = localStorage.getItem(STORAGE_KEY);

    if (state) {
        return JSON.parse(state);
    }
}

export function clearStorage() {
    localStorage.removeItem(STORAGE_KEY);
}

export const stateDemo = () => ({
    products: [
        {
            id: 15,
            name: 'Apple',
            inBasket: false,
            inList: false,
            categoryId: 5,
            quantity: 4,
            quantityUnit: ''
        },
        {
            id: 12,
            name: 'Banana',
            inBasket: false,
            inList: false,
            categoryId: 5,
            quantity: 1,
            quantityUnit: ''
        },
        {
            id: 3,
            name: 'Beer',
            inBasket: false,
            inList: false,
            categoryId: 1,
            quantity: 2,
            quantityUnit: 'l'
        },
        {
            id: 8,
            name: 'Bread',
            inBasket: false,
            inList: false,
            categoryId: 3,
            quantity: 1,
            quantityUnit: ''
        },
        {
            id: 16,
            name: 'Carrots',
            inBasket: false,
            inList: false,
            categoryId: 6,
            quantity: 500,
            quantityUnit: 'g'
        },
        {
            id: 9,
            name: 'Cheese',
            inBasket: false,
            inList: false,
            categoryId: 4,
            quantity: 200,
            quantityUnit: 'g'
        },
        {
            id: 1,
            name: 'Coca cola',
            inBasket: false,
            inList: false,
            categoryId: 2,
            quantity: 6,
            quantityUnit: ''
        },
        {
            id: 7,
            name: 'Donuts',
            inBasket: false,
            inList: false,
            categoryId: 3,
            quantity: 3,
            quantityUnit: ''
        },
        {
            id: 11,
            name: 'Eggs',
            inBasket: false,
            inList: false,
            categoryId: 4,
            quantity: 8,
            quantityUnit: ''
        },
        {
            id: 6,
            name: 'Jack Daniel\'s',
            inBasket: false,
            inList: false,
            categoryId: 1,
            quantity: 700,
            quantityUnit: 'ml'
        },
        {
            id: 2,
            name: 'Lemon Juice',
            inBasket: false,
            inList: false,
            categoryId: 2,
            quantity: 1,
            quantityUnit: ''
        },
        {
            id: 10,
            name: 'Milk',
            inBasket: false,
            inList: false,
            categoryId: 4,
            quantity: 2,
            quantityUnit: 'l'
        },
        {
            id: 0,
            name: 'Pepsi',
            inBasket: false,
            inList: false,
            categoryId: 2,
            quantity: 2,
            quantityUnit: ''
        },
        {
            id: 14,
            name: 'Pineapple',
            inBasket: false,
            inList: false,
            categoryId: 5,
            quantity: 1,
            quantityUnit: ''
        },
        {
            id: 13,
            name: 'Tomato',
            inBasket: false,
            inList: false,
            categoryId: 6,
            quantity: 5,
            quantityUnit: ''
        },
        {
            id: 5,
            name: 'Water',
            inBasket: false,
            inList: false,
            categoryId: 2,
            quantity: 10,
            quantityUnit: 'l'
        },
        {
            id: 4,
            name: 'White Wine',
            inBasket: false,
            inList: false,
            categoryId: 1,
            quantity: 1,
            quantityUnit: 'l'
        }
    ],
    categories: [
        {
            id: 0,
            name: 'None',
            color: '#000'
        },
        {
            id: 1,
            name: 'Alcohol',
            color: '#8e44ad'
        },
        {
            id: 2,
            name: 'Drinks',
            color: '#3498db'
        },
        {
            id: 3,
            name: 'Bread',
            color: '#e67e22'
        },
        {
            id: 4,
            name: 'Dairy',
            color: '#e74c3c'
        },
        {
            id: 5,
            name: 'Fruits',
            color: '#2ecc71'
        },
        {
            id: 6,
            name: 'Vegetables',
            color: '#f1c40f'
        }
    ],
    messageBar: {
        text: '',
        show: false,
        actions: [
            {
                text: 'Yes',
                callback: ()=>{},
            },
            {
                text: 'No',
                callback: ()=>{},
                type: 'secondary'
            },
        ]
    },
    productLastIndex: 17,
    sort: 'alphabetic',
    activeTab: 'all',
    editedProduct: null,
    editedCategory: null,
    showCategoriesEdit: false,
    showCategoryAdd: false
});

export const stateNew = () => ({
    products: [],
    categories: [
        {
            id: 0,
            name: 'None',
            color: '#a0a0a0'
        },
    ],
    messageBar: {
        text: '',
        show: false,
        actions: [
            {
                text: 'Yes',
                callback: ()=>{},
            },
            {
                text: 'No',
                callback: ()=>{},
                type: 'secondary'
            },
        ]
    },
    productLastIndex: 0,
    sort: 'alphabetic',
    activeTab: 'all',
    editedProduct: null,
    editedCategory: null,
    showCategoriesEdit: false,
    showCategoryAdd: false
});
