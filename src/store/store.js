import Vue from 'vue';
import Vuex from 'vuex';
import * as Storage from '../utilities/storage';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: Storage.stateDemo(),
    getters: {
        productsAll: state => {
            return state.products.sort((product1, product2) => {
                return product1.name.toLowerCase().localeCompare(product2.name.toLowerCase());
            });
        },
        productsAllCount: (state, getters) => {
            return getters.productsAll.length;
        },
        productsInList: state => {
            return state.products.filter(product => product.inList && !product.inBasket);
        },
        productsInListCount: (state, getters) => {
            return getters.productsInList.length;
        },
        productsInBasket: state => {
            return state.products.filter(product => product.inBasket);
        },
        productsInBasketCount: (state, getters) => {
            return getters.productsInBasket.length;
        },
        productsInListByCategory: (state, getters) => {
            return state.categories.map((category) => {
                const products = getters.productsInList.filter((product) => {
                    return product.categoryId === category.id;
                });
                return {
                    category,
                    products
                }
            });
        },
        productsInBasketByCategory: (state, getters) => {
            return state.categories.map((category) => {
                const products = getters.productsInBasket.filter((product) => {
                    return product.categoryId === category.id;
                });
                return {
                    category,
                    products
                }
            });
        },
        productsAllByCategory: (state, getters) => {
            return state.categories.map((category) => {
                const products = getters.productsAll.filter((product) => {
                    return product.categoryId === category.id;
                });
                return {
                    category,
                    products
                }
            });
        },
        findProductByName: (state) => (name) => {
            return state.products.find(product => product.name.toLowerCase() === name.toLowerCase());
        },
        queryProductsByName: (state, getters) => (query) => {
            query = query.trim();
            return getters.productsAll.filter(product => query !== '' && product.name.toLowerCase().includes(query.toLowerCase()));
        },
        getCategoriesWithProducts: (state, getters) => {
            return state.categories.map(category => {
                return {
                    ...category,
                    products: getters.productsAll.filter(product => product.categoryId === category.id)
                }
            });
        },
        getCategoriesSorted: (state) => {
            return [...state.categories].sort((category1, category2) => {
                // Sort all but first category
                if (category1.id === 0) {
                    return -1;
                }
                if (category2.id === 0) {
                    return 1;
                }
                return category1.name.toLowerCase().localeCompare(category2.name.toLowerCase());
            });
        },
        findCategoryByName: (state) => (name) => {
            return state.categories.find(category => category.name.toLowerCase() === name.toLowerCase());
        },
        findCategoryByProduct: (state) => (product) => {
            return state.categories.find(category => category.id === product.categoryId);
        }
    },
    mutations: {
        initialiseStore(state) {
            const store = Storage.loadStorage();

            if (store) {
                this.replaceState(Object.assign(state, store));
            }
        },
        productAdd(state, name) {
            state.products.push({
                id: state.productLastIndex++,
                name,
                inBasket: false,
                inList: false,
                categoryId: 0,
                quantity: 1,
                quantityUnit: ''
            });
        },
        productEdit(state, product) {
            state.editedProduct = product;
        },
        productSave(state, product) {
            state.products = [
                ...state.products.filter(element => element.id !== product.id),
                product
            ];
        },
        productRemove(state, product) {
            state.products = state.products.filter((prod) => {
                return prod.id !== product.id;
            });
        },
        dataClear(state) {
            Storage.clearStorage();
            this.replaceState(Object.assign(state, Storage.stateDemo()));
        },
        categoryEdit(state, category) {
            state.editedCategory = category;
        },
        toggleBasket(state, product) {
            const productFound = state.products.find(p => p.id === product.id);
            if( productFound ) {
                productFound.inBasket = !productFound.inBasket;
            }
        },
        addToList(state, product) {
            const productFound = state.products.find(p => p.id === product.id);
            if( productFound ) {
                productFound.inList = true;
                productFound.inBasket = false;
            }
        },
        removeFromList(state, product) {
            const productFound = state.products.find(p => p.id === product.id);
            if( productFound ) {
                productFound.inList = false;
                productFound.inBasket = false;
            }
        },
        setActiveTab(state, tab) {
            state.activeTab = tab;
        },
        setShowCategoriesEdit(state, show) {
            state.showCategoriesEdit = show;
        },
        showCategoryAdd(state, show) {
            state.showCategoryAdd = show;
        },
        categoryAdd(state, category) {
            const categoryLastIndex = state.categories.reduce((acc, cur) => {
                if (cur.id > acc) {
                    return cur.id;
                }
                return acc;
            }, 0);

            const categoryNew = {
                ...category,
                id: categoryLastIndex + 1
            };

            state.categories.push(categoryNew);
        },
        categorySave(state, category) {
            state.categories = state.categories.map((cat) => {
                if (cat.id === category.id) {
                    return category;
                }

                return cat;
            });
        },
        categoryRemove(state, category) {
            const categoryId = category.id;
            state.products
                .filter(product => product.categoryId === categoryId)
                .forEach(product => product.categoryId = 0);
            state.categories = state.categories.filter((category) => {
                return category.id !== categoryId;
            });
        },
        categoryMoveUp(state, index) {
            if (index > 0) {
                const category = state.categories[index];
                const categories = [...state.categories];
                categories.splice(index, 1);
                categories.splice(index - 1, 0, category);
                state.categories = categories;
            }

        },
        categoryMoveDown(state, index) {
            if (index < state.categories.length - 1) {
                const category = state.categories[index];
                const categories = [...state.categories];
                categories.splice(index, 1);
                categories.splice(index + 1, 0, category);
                state.categories = categories;
            }
        },
        setSortAlphabetic(state) {
            state.sort = 'alphabetic';
        },
        setSortGroup(state) {
            state.sort = 'group';
        },
        setMessageBar(state, message) {
            state.messageBar = Object.assign(state.messageBar, message);
            state.messageBar.show = true;
        },
        closeMessageBar(state) {
            state.messageBar.show = false;
        }
    }

});

export default store;